from transformers import pipeline

classifier = pipeline("sentiment-analysis", model="michellejieli/emotion_text_classifier")

def analyze_emotion(text):
    results = classifier(text)
    return results

if __name__ == "__main__":
    sample_text = "I love you"
    emotion_results = analyze_emotion(sample_text)
    print(emotion_results)
